#language: pt
@AllTest
Funcionalidade: Acessar Home Chopp Brahma Express
  Sendo um usuário
  Posso pesquisar produtos, comprar e etc...
  Para uso pessoal, dar ou doar a a lguem.

  Contexto: Acessar com a Home Chopp Brahma Express
    Dado que acesso a Home Chopp Brahma Express

  @PesquisaProduto
  Cenario: Pesquisar produto com sucesso
    Quando informo que possuo 18 anos
    Então sou direcionado para a home
