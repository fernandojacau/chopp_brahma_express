Dado('que acesso a Home Chopp Brahma Express') do
  brahma_express.load
end
Quando('informo que possuo {int} anos') do |int|
  brahma_express.mais_18
end

Então('sou direcionado para a home') do
  expect(page.current_url).to eql BASE_URL['base_url']
end
