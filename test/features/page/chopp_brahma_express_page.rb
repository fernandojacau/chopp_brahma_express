class ChoppBrahmaExpress < SitePrism::Page
  set_url ' '
  
  def mais_18
    if page.has_css?('#enter') != false
      find(ELL['bt_mais18']).click
    else
      click_button "SIM"
    end
  end
  
end